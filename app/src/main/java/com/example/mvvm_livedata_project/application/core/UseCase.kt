package com.example.mvvm_livedata_project.application.core

abstract class UseCase<P, R> {
	abstract suspend fun execute(request: P) : R
}