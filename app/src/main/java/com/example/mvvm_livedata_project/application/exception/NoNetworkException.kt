package com.example.mvvm_livedata_project.application.exception

import java.io.IOException

class NoNetworkException(message: String = "No Network Exception") : IOException(message)