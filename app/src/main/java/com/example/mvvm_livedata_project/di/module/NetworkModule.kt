package com.example.mvvm_livedata_project.di.module

import com.example.mvvm_livedata_project.application.network.ApiConfig
import com.example.mvvm_livedata_project.application.network.DefaultNetworkConnection
import com.example.mvvm_livedata_project.application.network.DefaultOkHttpClientService
import com.example.mvvm_livedata_project.application.network.DefaultServiceGenerator
import com.example.mvvm_livedata_project.application.network.NetworkConnection
import com.example.mvvm_livedata_project.application.network.OkHttpClientService
import com.example.mvvm_livedata_project.application.network.ServiceGenerator
import com.example.mvvm_livedata_project.application.network.interceptor.HttpNetworkInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module

val networkModule = module {
	single { ApiConfig("https://api.coinranking.com/v1/public/")}
	single { HttpNetworkInterceptor(get()) }
	single<NetworkConnection> { DefaultNetworkConnection(androidContext()) }
	single<OkHttpClientService> { DefaultOkHttpClientService() }
	single<ServiceGenerator> {
		DefaultServiceGenerator(
				okHttpClientService = get(),
				httpNetworkInterceptor = get()
		)
	}
}