package com.example.mvvm_livedata_project.di

import com.example.mvvm_livedata_project.di.module.networkModule
import com.example.mvvm_livedata_project.ui.mainModule

var contributorModule = listOf(
		networkModule,
		mainModule
)