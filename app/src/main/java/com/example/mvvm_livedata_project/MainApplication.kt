package com.example.mvvm_livedata_project

import android.app.Application
import com.example.mvvm_livedata_project.di.contributorModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber

class MainApplication : Application() {
	override fun onCreate() {
		super.onCreate()
		setupKoin()
		Timber.plant(Timber.DebugTree())
	}

	private fun setupKoin() {
		startKoin {
			androidContext(this@MainApplication)
			modules(contributorModule)
			androidLogger()
		}
	}
}