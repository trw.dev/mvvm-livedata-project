package com.example.mvvm_livedata_project.ui.presenter

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import com.example.mvvm_livedata_project.R
import kotlinx.android.synthetic.main.activity_main.*
import org.koin.androidx.scope.lifecycleScope
import org.koin.androidx.viewmodel.scope.viewModel

class MainActivity : AppCompatActivity() {

	private val mainViewModel: MainViewModel by lifecycleScope.viewModel(this)

	override fun onCreate(savedInstanceState: Bundle?) {
		super.onCreate(savedInstanceState)
		setContentView(R.layout.activity_main)
		getCoins()
	}

	private fun getCoins() {
		mainViewModel.getCoins()
		mainViewModel.coinsDataSuccess.observe(this, Observer {
			textView.text = it.data?.coins?.get(0)?.name ?: "EIEI"
		})
		mainViewModel.coinsDataError.observe(this, Observer {

		})
		mainViewModel.coinsShowLoading.observe(this, Observer {

		})
		mainViewModel.coinsHiddenLoading.observe(this, Observer {

		})
	}


}
