package com.example.mvvm_livedata_project.ui.datasource

import com.example.mvvm_livedata_project.application.network.Results
import com.example.mvvm_livedata_project.ui.domain.CoinsData

interface MainRepository {
	suspend fun getCoinList(): Results<CoinsData>
}

class MainRepositoryImpl(private val mainDataSource: MainDataSource) : MainRepository {

	override suspend fun getCoinList(): Results<CoinsData> {
		return mainDataSource.getCoins()
	}

}