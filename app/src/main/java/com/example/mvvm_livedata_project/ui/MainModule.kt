package com.example.mvvm_livedata_project.ui

import com.example.mvvm_livedata_project.application.network.ServiceGenerator
import com.example.mvvm_livedata_project.ui.datasource.MainDataSource
import com.example.mvvm_livedata_project.ui.datasource.MainRepository
import com.example.mvvm_livedata_project.ui.datasource.MainRepositoryImpl
import com.example.mvvm_livedata_project.ui.datasource.remote.MainApi
import com.example.mvvm_livedata_project.ui.datasource.remote.MainRemoteDataSource
import com.example.mvvm_livedata_project.ui.domain.GetCoinsUseCase
import com.example.mvvm_livedata_project.ui.domain.GetCoinsUseCaseImpl
import com.example.mvvm_livedata_project.ui.presenter.MainActivity
import com.example.mvvm_livedata_project.ui.presenter.MainViewModel
import org.koin.androidx.experimental.dsl.viewModel
import org.koin.dsl.module

val mainModule = module {
	scope<MainActivity> {
		factory<MainRepository> { MainRepositoryImpl(get()) }
		factory<GetCoinsUseCase> { GetCoinsUseCaseImpl(get()) }
		factory<MainDataSource> { MainRemoteDataSource(get()) }
		viewModel<MainViewModel>()
		factory { get<ServiceGenerator>().create(get(), MainApi::class.java) }
	}
}