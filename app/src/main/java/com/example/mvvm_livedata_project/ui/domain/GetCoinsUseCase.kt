package com.example.mvvm_livedata_project.ui.domain

import com.example.mvvm_livedata_project.application.core.UseCase
import com.example.mvvm_livedata_project.application.network.Results
import com.example.mvvm_livedata_project.ui.datasource.MainRepository

abstract class GetCoinsUseCase : UseCase<Unit, Results<CoinsData>>()

class GetCoinsUseCaseImpl(private val repository: MainRepository) : GetCoinsUseCase() {

	override suspend fun execute(request: Unit): Results<CoinsData> {
		return repository.getCoinList()
	}

}