package com.example.mvvm_livedata_project.ui.presenter

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.viewModelScope
import com.example.mvvm_livedata_project.application.core.BaseViewModel
import com.example.mvvm_livedata_project.application.network.Results
import com.example.mvvm_livedata_project.ui.domain.CoinsData
import com.example.mvvm_livedata_project.ui.domain.GetCoinsUseCase
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch

class MainViewModel(private val getCoinsUseCase: GetCoinsUseCase) : BaseViewModel() {

	val coinsDataSuccess = MutableLiveData<CoinsData>()
	val coinsDataError = MutableLiveData<String>()
	val coinsShowLoading = MutableLiveData<Unit>()
	val coinsHiddenLoading = MutableLiveData<Unit>()

	fun getCoins() {
		viewModelScope.launch(Dispatchers.Main) {
			coinsShowLoading.postValue(Unit)
			when (val results = getCoinsUseCase.execute(Unit)) {
				is Results.Success -> {
					coinsHiddenLoading.postValue(Unit)
					coinsDataSuccess.value = results.data
				}
				is Results.Error -> {
					coinsHiddenLoading.postValue(Unit)
					coinsDataError.postValue(results.exception.message)
				}
			}
		}
	}

}