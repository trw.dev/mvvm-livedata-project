package com.example.mvvm_livedata_project.ui.datasource.remote

import com.example.mvvm_livedata_project.application.network.Results
import com.example.mvvm_livedata_project.application.network.safeApiCall
import com.example.mvvm_livedata_project.ui.datasource.MainDataSource
import com.example.mvvm_livedata_project.ui.domain.CoinsData

class MainRemoteDataSource(private val api: MainApi) : MainDataSource {

	override suspend fun getCoins(): Results<CoinsData> {
		return safeApiCall {
			api.getCoins()
		}
	}
}