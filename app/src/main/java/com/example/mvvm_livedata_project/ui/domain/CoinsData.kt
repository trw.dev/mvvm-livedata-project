package com.example.mvvm_livedata_project.ui.domain

import android.os.Parcelable
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
data class CoinsData(
		@SerializedName("data")
		val data: Data?,
		@SerializedName("status")
		val status: String?
) : Parcelable {
	@Parcelize
	data class Data(
			@SerializedName("coins")
			val coins: List<Coin?>?,
			@SerializedName("stats")
			val stats: Stats?
	) : Parcelable {
		@Parcelize
		data class Coin(
				@SerializedName("allTimeHigh")
				val allTimeHigh: AllTimeHigh?,
				@SerializedName("change")
				val change: Double,
				@SerializedName("circulatingSupply")
				val circulatingSupply: Double,
				@SerializedName("color")
				val color: String?,
				@SerializedName("confirmedSupply")
				val confirmedSupply: Boolean,
				@SerializedName("description")
				val description: String?,
				@SerializedName("firstSeen")
				val firstSeen: Long,
				@SerializedName("history")
				val history: List<String?>?,
				@SerializedName("iconType")
				val iconType: String?,
				@SerializedName("iconUrl")
				val iconUrl: String?,
				@SerializedName("id")
				val id: Int,
				@SerializedName("marketCap")
				val marketCap: Long,
				@SerializedName("name")
				val name: String?,
				@SerializedName("numberOfExchanges")
				val numberOfExchanges: Int,
				@SerializedName("numberOfMarkets")
				val numberOfMarkets: Int,
				@SerializedName("penalty")
				val penalty: Boolean,
				@SerializedName("price")
				val price: String?,
				@SerializedName("rank")
				val rank: Int,
				@SerializedName("slug")
				val slug: String?,
				@SerializedName("socials")
				val socials: List<Social?>?,
				@SerializedName("symbol")
				val symbol: String?,
				@SerializedName("totalSupply")
				val totalSupply: Double,
				@SerializedName("type")
				val type: String?,
				@SerializedName("volume")
				val volume: Long,
				@SerializedName("websiteUrl")
				val websiteUrl: String?
		) : Parcelable {
			@Parcelize
			data class AllTimeHigh(
					@SerializedName("price")
					val price: String?,
					@SerializedName("timestamp")
					val timestamp: Long
			) : Parcelable

			@Parcelize
			data class Social(
					@SerializedName("name")
					val name: String?,
					@SerializedName("type")
					val type: String?,
					@SerializedName("url")
					val url: String?
			) : Parcelable
		}

		@Parcelize
		data class Stats(
				@SerializedName("base")
				val base: String?,
				@SerializedName("limit")
				val limit: Int,
				@SerializedName("offset")
				val offset: Int,
				@SerializedName("order")
				val order: String?,
				@SerializedName("total")
				val total: Int
		) : Parcelable
	}
}