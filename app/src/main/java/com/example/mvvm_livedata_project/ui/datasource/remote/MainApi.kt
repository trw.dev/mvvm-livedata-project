package com.example.mvvm_livedata_project.ui.datasource.remote

import com.example.mvvm_livedata_project.ui.domain.CoinsData
import retrofit2.Response
import retrofit2.http.GET

interface MainApi {
	@GET("coins/")
	suspend fun getCoins(): Response<CoinsData>
}