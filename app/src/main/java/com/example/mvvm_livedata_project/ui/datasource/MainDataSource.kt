package com.example.mvvm_livedata_project.ui.datasource

import com.example.mvvm_livedata_project.application.network.Results
import com.example.mvvm_livedata_project.ui.domain.CoinsData

interface MainDataSource {
	suspend fun getCoins(): Results<CoinsData>
}